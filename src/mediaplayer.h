/*
 * Copyright (C) 2013 Canonical, Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIAPLAYER_H
#define MEDIAPLAYER_H

#include <QApplication>
#include <QQuickView>
#include <QSettings>
#include <QUrl>

class MediaPlayer : public QApplication {
    Q_OBJECT
    Q_PROPERTY(QUrl playUri READ playUri WRITE setPlayUri NOTIFY playUriChanged)
    Q_PROPERTY(bool windowed READ windowed CONSTANT)

public:
    MediaPlayer(int& argc, char** argv);
    virtual ~MediaPlayer();

    bool setup();
    QUrl playUri() { return m_playUri; }
    void setPlayUri(QUrl uri);
    bool windowed();

    Q_INVOKABLE int getStoredPosition();
    Q_INVOKABLE void storePosition(int duration);
    Q_INVOKABLE bool isAudio(const QString& file);

signals:
    void playUriChanged();

public Q_SLOTS:
    void toggleFullscreen();
    void leaveFullScreen();
    void onWidthChanged(int);
    void onHeightChanged(int);
    QList<QUrl> copyFiles(const QList<QUrl>& urls);

private:
    QQuickView* m_view;
    QSettings m_settings;
    QUrl m_playUri;
    bool m_windowed;
};

#endif // MEDIAPLAYER_H
