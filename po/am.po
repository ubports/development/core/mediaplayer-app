# Amharic translation for mediaplayer-app
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the mediaplayer-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: mediaplayer-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-10 13:58+0000\n"
"PO-Revision-Date: 2014-11-09 18:48+0000\n"
"Last-Translator: samson <Unknown>\n"
"Language-Team: Amharic <am@li.org>\n"
"Language: am\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2017-04-05 07:16+0000\n"
"X-Generator: Launchpad (build 18335)\n"

#: mediaplayer-app.desktop.in:4 mediaplayer-app.desktop.in:5
#: mediaplayer-app.desktop.in:6 src/mediaplayer.cpp:82
msgid "Media Player"
msgstr "ምስል እና ድምፅ ማጫወቻ"

#: mediaplayer-app.desktop.in:7
msgid "Movies;Movie Player;Video Player"
msgstr ""

#: mediaplayer-app.desktop.in:10
msgid "/usr/share/mediaplayer-app/mediaplayer-app.png"
msgstr ""

#: src/qml/player.qml:76
msgid "Error"
msgstr "ስህተት"

#: src/qml/player.qml:77
msgid ""
"No video selected to play. Connect your phone to your computer to transfer "
"videos to the phone. Then select video from Videos scope."
msgstr ""
"ምንም ቪዲዮ አልተመረጠም ለማጫወት: ስልኩን ከ ኮምፒዩተር ጋር ያገናኙ እና ቪዲዮ ወደ ስልኩ ያስተላልፉ ከዛ ቪዲዮ "
"ይምረጡ ከ ቪዲዮ ክልል ውስጥ"

#: src/qml/player.qml:80
msgid "Ok"
msgstr "እሺ"

#: src/qml/player.qml:188
msgid "Play / Pause"
msgstr "ማጫወቻ / ማስቆሚያ"

#: src/qml/player.qml:189
msgid "Pause or Resume Playhead"
msgstr "ማጫወቻውን ማስቆሚያ ወይንም መቀጠያ"

#: src/qml/player.qml:193
msgid "Share"
msgstr "ማካፈያ"

#: src/qml/player.qml:194
msgid "Post;Upload;Attach"
msgstr "መለጠፊያ:መጫኛ:ማያያዣ:"

#. TRANSLATORS: this refers to a duration/remaining time of the video, of which you can change the order.
#. %1 refers to hours, %2 refers to minutes and %3 refers to seconds.
#: src/qml/player/TimeLine.qml:55
#, qt-format
msgid "%1:%2:%3"
msgstr "%1:%2:%3"

#. TRANSLATORS: this refers to an unknown duration.
#: src/qml/player/TimeLine.qml:85
msgid "unknown"
msgstr "ያልታወቀ"

#: src/qml/player/TimeLine.qml:88
msgid "0:00:00"
msgstr "0:00:00"

#: src/qml/player/VideoPlayer.qml:195
msgid "Please choose a file to open"
msgstr ""

#: src/qml/player/VideoPlayer.qml:298
msgid "Error playing video"
msgstr "ቪዲዮ በማጫወት ላይ እንዳለ ስህተት ተፈጥሯል"

#: src/qml/player/VideoPlayer.qml:302
msgid "Close"
msgstr "መዝጊያ"

#: src/qml/player/VideoPlayer.qml:325
msgid "Fail to open the source video."
msgstr "የ ቪዲዮውን ምንጭ መክፈት አልተቻለም"

#: src/qml/player/VideoPlayer.qml:328
msgid "Video format not supported."
msgstr "የ ቪዲዮው አቀራረብ የተደገፈ አይደለም"

#: src/qml/player/VideoPlayer.qml:331
msgid "A network error occurred."
msgstr "የ ኔትዎርክ ስህተት ተፈጥሯል"

#: src/qml/player/VideoPlayer.qml:334
#, fuzzy
#| msgid "There are not the appropriate permissions to play a media resource."
msgid "You don't have the appropriate permissions to play a media resource."
msgstr "የ መገናኛ ምንጩን ለማጫወት በቂ የሆነ ፍቃድ አልተገኘም"

#: src/qml/player/VideoPlayer.qml:337
msgid "Fail to connect with playback backend."
msgstr "መገናኘት አልተቻለም የተቀመጠውን መልሶ ለማጫወት"

#~ msgid "Open Video"
#~ msgstr "ቪዲዮ መክፈቻ"

#~ msgid ""
#~ "Video files (*.avi *.mov *.mp4 *.divx *.ogg *.ogv *.mpeg);;All files (*)"
#~ msgstr ""
#~ "ቪዲዮ ፋይሎች (*.avi *.mov *.mp4 *.divx *.ogg *.ogv *.mpeg);;ሁሉም ፋይሎች (*)"
